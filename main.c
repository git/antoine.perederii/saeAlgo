#include "Fonctions.h"

void testOuverture(void)
{
    int tNoCarte[20] = {0}, tage[20] = {0}, tPointsCarte[20] = {0}, tCarteActive[20] = {0};
    int tDate[20] = {0}, tNbActivitesJour[20] = {0};
    int tmax = 20, pasMembres, pasAct, i;
    pasMembres = Ouverture(tNoCarte, tage, tPointsCarte, tCarteActive, tNbActivitesJour, tDate, tmax, &pasAct);
    printf("Pas membre: %d\nPas Act : %d\n", pasMembres, pasAct);
    printf("Ncarte age pointsCarte carteActive nbActivitesJour    Date taillemax\n");
    for(i=0; i < pasMembres; i++)
        printf("%d\t%d\t%d\t%d\t\t%d\t\t%d\t%d\n", tNoCarte[i], tage[i], tPointsCarte[i], tCarteActive[i], tNbActivitesJour[i], tDate[i], tmax);
}

void testSauvegarde(void)
{
    int tNoCarte[500] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, tage[500] = {22, 25, 47, 38, 18, 46, 36, 19, 21, 30}, tPointsCarte[500] = {10, 5, 7, 8, 8, 6, 6, 2, 1, 4}, tCarteActive[500] = {0, 1, 1, 1, 1, 1, 0, 1, 1, 1};
    int tDate[500] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}, tNbActivitesJour[500] = {500, 525, 740, 812, 850, 620, 601, 422, 501, 520};
    Sauvegarde(tNoCarte, tage, tPointsCarte, tCarteActive, tNbActivitesJour, tDate, 10, 10);
}

int main(void)
{
    //testOuverture();
    //testSauvegarde();
    GestionMenus();
    return 0;
}