/**
 * \file Fonctions.h
 * \author Antoine Perederii - Ayour Malki - Kyllian Chabanon
 * \brief Fichier contenant les prototypes de chaque fonction.
*/

#include <stdio.h>
#include <stdlib.h>

/*  Fonctions d'ouverture des fichiers texte  */
int Ouverture(int tNoCarte[], int tAge[], int tPointsCarte[], int tCarteActive[], int tNbActivitesJour[], int tDate[], int tMax, int *pasAct);
int OuvertureMembres(int tNoCarte[], int tAge[], int tPointsCarte[], int tCarteActive[], int tMax);
int OuvertureActivitesJour(int tNbActivitesJour[], int tDate[], int tMax);

/*  Fonctions des menus  */
int ChoixMenu(void);
int ChoixMenuAffichage(void);
int choixMenuActivites(void);

/*  Fonctions d'affichage  */
void Affichage1Adherent(int tNoCarte[], int tAge[], int tPointsCarte[], int tCarteActive[], int nbElem);
void AffichageTousAdherents(int tNoCarte[], int tAge[], int tPointsCarte[], int tCarteActive[], int nbElem);
void AffichageNbEntreesTousJour(int tNbActivitesJour[], int tDate[], int nbElem);
void AffichageNbEntreesTotal(int tNbActivitesJour[], int nbElem);
void AffichageNbAdherents(int nbElem);

/*  Fonction globale  */
void GestionMenus(void);

/*  Fonction de sauvegarde  */
int Sauvegarde(int tNoCarte[], int tAge[], int tPointsCarte[], int tCarteActive[], int tNbActivitesJour[], int tDate[], int nbElem, int pasAct);

/*  Fonctions de gestion des adhérents  */
int RechercheAdherent(int tNoCarte[], int nbElem, int noCarte, int *trouve);
int RechercheAjoutAdherent(int tNoCarte[], int nbElem);
int AjoutAdherent(int tNoCarte[], int tAge[], int tPointsCarte[], int tCarteActive[], int tMax, int nbElem);
void ModificationAge(int tNoCarte[], int tAge[], int nbElem);
int SupprimerAdherent(int tNoCarte[], int tAge[], int tPointsCarte[], int tCarteActive[], int nbElem);
void ModificationActivationCarte(int tNoCarte[], int tCarteActive[], int nbElem);

/*  Fonctions de gestion des points  */
void AjoutPoints(int tNoCarte[], int tPointsCarte[], int tCarteActive[], int nbElem, int noCarte);
void DebitCarte(int tNoCarte[], int tPointsCarte[], int tCarteActive[], int tNbActivitesJour[], int nbElem);