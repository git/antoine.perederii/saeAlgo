/**
 * \file Sauvegarde.c
 * \brief Fichier contenant la fonction de sauvegarde
 * \author Antoine Perederii
*/

#include "Fonctions.h"

/**
 * \brief Sauvegarde les nouvelles données dans les fichiers
 * \author Antoine Perederii - Kyllian Chabanon
 * \param tNoCarte tableau contenant les numéros de toutes les cartes
 * \param tAge tableau contenant les âges des membres 
 * \param tPointsCarte tableau contenant le nombre de points restants sur chaque carte
 * \param tCarteActive tableau contenant l'état de chaque carte
 * \param tNbActivitesJour tableau contenant le nombre d'activité par jour
 * \param tDate tableau contenant les dates
 * \param nbElem taille logique des tableaux tNoCarte, tAge, tPointsCarte et tCarteActive
 * \param pasAct taille logique des tableaux tNbActivitesJour et tDate
 * \return le code d'erreur : -1 en cas d'erreur et 0 sinon
*/
int Sauvegarde(int tNoCarte[], int tAge[], int tPointsCarte[], int tCarteActive[], int tNbActivitesJour[], int tDate[], int nbElem, int pasAct)
{
    int i, j;
    FILE *membres, *jour;
    membres = fopen("membres.don", "w");
    jour = fopen("ActivitesJour.don", "w");
    if (membres == NULL)
    {
        printf("Problème d'ouverture du fichier membres.don en écriture.\n");
        return -1;
    }
    if (jour == NULL)
    {
        printf("Problème d'ouverture du fichier ActivitesJour.don en écriture.\n");
        return -1;
    }
    for (i = 0; i < nbElem; i++)
    {
        fprintf(membres, "%d\t%d\t\t%d\t\t%d\n", tNoCarte[i], tAge[i], tPointsCarte[i], tCarteActive[i]);
    }
    for (j = 0; j < pasAct; j++)
    {
        fprintf(jour, "%d\t\t%d\n", tDate[j], tNbActivitesJour[j]);
    }
    fclose(jour);
    fclose(membres);
    return 0;
}