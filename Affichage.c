/**
 * \file Affichage.c
 * \brief Fichier contenant toutes les fonctions d'affichage
 * \author Antoine Perederii - Ayour Malki
*/

#include "Fonctions.h"

/**
 * \brief Affiche les informations d'un adhérent
 * \author Antoine Perederii - Ayour Malki
 * \param tNoCarte tableau contenant les numéros de toutes les cartes
 * \param tAge tableau contenant les âges des membres
 * \param tPointsCarte tableau contenant le nombre de points restants sur chaque carte
 * \param tCarteActive tableau contenant l'état de chaque carte
 * \param nbElem taille logique des tableaux
 * 
 * Affiche le numéro de carte, l'âge, le nombre de points restants et l'état de la carte d'un adhérent.
*/
void Affichage1Adherent(int tNoCarte[], int tAge[], int tPointsCarte[], int tCarteActive[], int nbElem)
{
    int pas, noCarte, CarteActive, trouve;
    printf("Entrez le numéro de la carte de l'adhérent recherché : ");
    scanf("%d", &noCarte);
    pas = RechercheAdherent(tNoCarte, nbElem, noCarte, &trouve);
    if (trouve == 1)
    {
        printf("\nN° de carte\tAge\tPoints\tEtat\n");
        printf("%d\t\t%d\t%d\t%d\n", tNoCarte[pas], tAge[pas], tPointsCarte[pas], tCarteActive[pas]);
    }
    else
    {
        printf("Ce numéro d'adhérent n'existe pas. Veuillez réessayer\n");
        return;
    }
}

/**
 * \brief Affiche les informations de tous les adhérents
 * \author Antoine Perederii - Ayour Malki
 * \param tNoCarte tableau contenant les numéros de toutes les cartes
 * \param tAge tableau contenant les âges des membres
 * \param tPointsCarte tableau contenant le nombre de points restants sur chaque carte
 * \param tCarteActive tableau contenant l'état de chaque carte
 * \param nbElem taille logique des tableaux
 * 
 * Affiche le numéro de carte, l'âge, le nombre de points restants et l'état de la carte de l'adhérent recherché.
*/
void AffichageTousAdherents(int tNoCarte[], int tAge[], int tPointsCarte[], int tCarteActive[], int nbElem)
{
    int i;
    printf("\nN° de carte\tAge\tPoints\tEtat\n");
    for (i = 0; i < nbElem; i++)
    {
        printf("%d\t\t%d\t%d\t%d\n", tNoCarte[i], tAge[i], tPointsCarte[i], tCarteActive[i]);
    }
}

/**
 * \brief Affiche le nombre d'entrées de chaque jour
 * \author Antoine Perederii
 * \param tNbActivitesJour tableau contenant le nombre d'activités de chaque jour
 * \param tDate tableau contenant les dates
 * \param nbElem taille logique des tableaux
*/
void AffichageNbEntreesTousJour(int tNbActivitesJour[], int tDate[], int nbElem)
{
    int i;
    printf("\nDate\tNb activités\n");
    for (i = 0; i < nbElem; i++)
    {
        printf("%d\t%d\n", tDate[i], tNbActivitesJour[i]);
    }
}

/**
 * \brief Affiche le nombre d'entrées total
 * \author Ayour Malki
 * \param tNbActivitesJour tableau contenant le nombre d'activités de chaque jour
 * \param nbElem taille logique du tableau
*/
void AffichageNbEntreesTotal(int tNbActivitesJour[], int nbElem)
{
    int i, somme = 0;
    for (i = 0; i < nbElem; i++)
    {
        somme = somme + tNbActivitesJour[i];
    }
    printf("Le nombre total d'entrées est de %d depuis le début.\n", somme);
}

/**
 * \brief Affiche le nombre d'adhérents
 * \author Ayour Malki
 * \param nbElem taille logique des tableaux
*/
void AffichageNbAdherents(int nbElem)
{
    int i, somme = 0;
    for (i = 0; i < nbElem; i++)
    {
        somme = somme + 1;
    }
    printf("Le nombre total d'adhérents est de %d.\n", somme);
}