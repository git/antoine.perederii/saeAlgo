/**
 * \file Menus.c
 * \brief Fichier qui contient les fonctions du menu
 * \author Antoine Perederii - Ayour Malki - Kyllian Chabanon
*/

#include "Fonctions.h"

/**
 * \brief Affiche le menu principal et permet de choisir une option
 * \author Antoine Perederii - Ayour Malki - Kyllian Chabanon
 * \return le choix entré par l'utilisateur
*/
int ChoixMenu(void)
{
    int choix;
    printf("\nMenu principal\n");
    printf("1.\tAffichage\n");
    printf("2.\tAjouter un adhérent\n");
    printf("3.\tModifier l'âge d'un adhérent\n");
    printf("4.\tSuppression d'un adhérent\n");
    printf("5.\tAjout de points sur une carte\n");
    printf("6.\tModification de l'état de la carte\n");
    printf("7.\tCréditer une activité à une carte\n");
    printf("8.\tQuitter\n");
    printf("\nOption choisie : ");
    scanf("%d", &choix);
    return choix;
}

/**
 * \brief Affiche le menu d'affichage et permet de choisir une option
 * \author Antoine Perederii - Ayour Malki - Kyllian Chabanon
 * \return le choix entré par l'utilisateur
*/
int ChoixMenuAffichage(void)
{
    char choix;
    printf("\nMenu d'affichage\n");
    printf("A.\tAffichage des informations d'un adhérent\n");
    printf("B.\tAffichage des informations de tous les adhérents\n");
    printf("C.\tAffichage du nombre d’entrées totales de toutes les journées\n");
    printf("D.\tAffichage du nombre d'entrées totales depuis le début\n");
    printf("E.\tAffichage du nombre total d'adhérents\n");
    printf("F.\tQuitter\n");
    printf("\nOption choisie : ");
    scanf("%*c%c", &choix);
    return choix;
}

/**
 * \brief Affiche le menu d'affichage et permet de choisir une option
 * \author Antoine Perederii - Kyllian Chabanon
 * \return le choix entré par l'utilisateur
*/
int choixMenuActivites(void)
{
    int choix;
    printf("\nChoix des activités :\n");
    printf("1.\tPiscine\t\t\t0 points\n");
    printf("2.\tOption accessibilité\t\t0 points\n");
    printf("3.\tAquakid + Aquaplouf\t\t5 points\n");
    printf("4.\tAquafit\t\t\t9 points\n");
    printf("5.\tCours de natation\t\t10 points\n");
    printf("6.\tCours de gymnastique aquatique\t12 points\n");
    printf("7.\tVagues\t\t\t15 points\n");
    printf("8.\tAquabike\t\t\t20 points\n");
    printf("9.\tAquafamily\t\t\t35 points\n");
    printf("10.\tQuitter\n");
    printf("\nOption choisie : ");
    scanf("%d", &choix);
    return choix;
}