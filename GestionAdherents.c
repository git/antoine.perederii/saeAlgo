/**
 * \file GestionAdherents.c
 * \brief Fichier contenant les fonctions de gestion des adhérents
 * \author Antoine Perederii - Ayour Malki
*/

#include "Fonctions.h"

/**
 * \brief Recherche un adhérent pour la fonction AjoutAdherent
 * \author Antoine Perederii
 * \param tNoCarte tableau contenant les numéros de toutes les cartes
 * \param nbElem taille logique du tableau
 * \param noCarte numéro de la carte à rechercher
*/
int RechercheAjoutAdherent(int tNoCarte[], int nbElem)
{
    int i;
    while(i < nbElem)
    {
        if(tNoCarte[i] != i + 1)
        {
            return i;
        }
        i++;
    }
}

/**
 * \brief Affiche les informations d'un adhérent
 * \author Antoine Perederii
 * \param tNoCarte tableau contenant les numéros de toutes les cartes
 * \param tAge tableau contenant les âges des membres
 * \param tPointsCarte tableau contenant le nombre de points restants sur chaque carte
 * \param tCarteActive tableau contenant l'état de chaque carte
 * \param tMax taille physique des tableaux
 * \param nbElem taille logique des tableaux
 * \return la nouvelle taille logique des tableaux
 * 
 * Permet de créer un nouvel adhérent. Il est nécessaire de renseigner son âge. Lors de la création, il est demandé de créditer la carte.
*/
int AjoutAdherent(int tNoCarte[], int tAge[], int tPointsCarte[], int tCarteActive[], int tMax, int nbElem)
{
    int noCarte, age, pointsCarte, pas, trouve, j;
    char reponse;
    printf("Vous voulez créer un nouvel adhérent.\n");
    printf("Donnez l'âge de l'adhérent : ");
    scanf("%d", &age);
    pas = RechercheAjoutAdherent(tNoCarte, nbElem);
    if (j == tMax)
    {
        printf("Tableau plein, impossible d'ajouter un adhérent.\n"); /*    Pas besoins de le mettre dans la boucle for */
        return -1;
    }
    for (j = nbElem; j > pas; j--)
    {
        tNoCarte[j] = tNoCarte[j - 1];
        tAge[j] = tAge[j - 1];
        tPointsCarte[j] = tPointsCarte[j - 1];
        tCarteActive[j] = tCarteActive[j - 1];
    }
    tNoCarte[pas] = pas + 1;
    tAge[pas] = age;
    tPointsCarte[pas] = 0;
    tCarteActive[pas] = 0;
    nbElem++;
    printf("Vous avez créé l'adhérent numéro %d. Il a %d ans.\nSa carte n'est pas active car il n'y a pas de points dessus.\nIl est nécessaire de créditer la carte.\n", tNoCarte[pas], tAge[pas]);
    AjoutPoints(tNoCarte, tPointsCarte, tCarteActive, nbElem, tNoCarte[pas]);
    tCarteActive[pas] = 1;
    return nbElem;
}

/**
 * \brief Modifie l'âge d'un adhérent
 * \author Kyllian Chabanon
 * \param tNoCarte tableau contenant les numéros de toutes les cartes
 * \param tAge tableau contenant les âges des membres
 * \param nbElem taille logique des tableaux
 * 
 * Permet de modifier l'âge de l'adhérent recherché.
*/
void ModificationAge(int tNoCarte[], int tAge[], int nbElem)
{
    int pas, noCarte, age, trouve;
    printf("Entrez le numéro de la carte de l'adhérent recherché : ");
    scanf("%d", &noCarte);
    pas = RechercheAdherent(tNoCarte, nbElem, noCarte, &trouve);
    if (trouve == 1)
    {
        printf("Entrez le nouvel âge de l'adhérent : ");
        scanf("%d", &age);
        tAge[pas] = age;
        printf("Vous avez modifié l'âge de l'adhérent avec le numéro de carte %d. Son nouvel âge est de %d ans.\n", noCarte, age);
    }
    else
    {
        printf("Ce numéro d'adhérent n'existe pas. Veuillez réessayer.\n");
        return;
    }
}

/**
 * \brief Supprime un adhérent
 * \author Antoine Perederii
 * \param tNoCarte tableau contenant les numéros de toutes les cartes
 * \param tAge tableau contenant les âges des membres
 * \param tPointsCarte tableau contenant le nombre de points restants sur chaque carte
 * \param tCarteActive tableau contenant l'état de chaque carte
 * \param nbElem taille logique des tableaux
 * \return la nouvelle taille logique des tableaux
*/
int SupprimerAdherent(int tNoCarte[], int tAge[], int tPointsCarte[], int tCarteActive[], int nbElem)
{
    int pas, i, noCarte, trouve;
    printf("Entrez le numéro de la carte de l'adhérent recherché : ");
    scanf("%d", &noCarte);
    pas = RechercheAdherent(tNoCarte, nbElem, noCarte, &trouve);
    if (trouve == 1)
    {
        for (i = pas; i < nbElem - 1 ; i++)
        {
            tNoCarte[i] = tNoCarte[i + 1];
            tAge[i] = tAge[i + 1];
            tPointsCarte[i] = tPointsCarte[i + 1];
            tCarteActive[i] = tCarteActive[i + 1];
        }
        nbElem = nbElem - 1;
        printf("Vous avez bien supprimé l'adhérent numéro %d.\n", noCarte);
        return nbElem;
    }
    else
    {
        printf("Ce numéro d'adhérent n'existe pas. Veuillez réessayer.\n");
        return -1;
    }
}

/**
 * \brief Modifie l'état de la carte
 * \author Antoine Perederii - Kyllian Chabanon
 * \param tNoCarte tableau contenant les numéros de toutes les cartes
 * \param tCarteActive tableau contenant l'état de chaque carte
 * \param nbElem taille logique des tableaux
 * 
 * Permet de modifier l'état de la carte de l'adhérent recherché.
 * L'état peut être 1 si la carte est activée et 0 si elle est désactivée.
 * Il est possible de choisir la raison de l'activation et de la désactivation de la carte.
*/
void ModificationActivationCarte(int tNoCarte[], int tCarteActive[], int nbElem)
{
    int noCarte, trouve, pas, choix, choixRaison;
    printf("Entrez le numéro de la carte de l'adhérent recherché : ");
    scanf("%d", &noCarte);
    pas = RechercheAdherent(tNoCarte, nbElem, noCarte, &trouve);
    if (trouve == 1)
    {
        printf("\nQue voulez-vous faire ?\n1.\tActiver la carte\n2.\tDésactiver la carte\n");
        printf("\nOption choisie :");
        scanf("%d", &choix);
        if (choix == 1)
        {
            printf("\nPourquoi voulez-vous activer la carte ?\n1.\tCarte retrouvée\n2.\tNouvelle carte à la place de l'ancienne\n3.\tLevée de sanction\n");
            printf("\nOption choisie : ");
            scanf("%d", &choixRaison);

            if (tCarteActive[pas] == 1)
            {
                printf("La carte est déjà activée.\n");
                return;
            }
            tCarteActive[pas] = 1;
            printf("La carte numéro %d est désormais activée.\n", noCarte);
        }
        else if (choix == 2)
        {
            printf("\nPourquoi voulez-vous désactiver la carte ?\n1.\tPerte\n2.\tVol\n3.\tSanction\n");
            printf("\nOption choisie : ");
            scanf("%d", &choixRaison);

            if (tCarteActive[pas] == 0)
            {
                printf("La carte est déjà désactivée.\n");
                return;
            }
            tCarteActive[pas] = 0;
            printf("La carte numéro %d est désormais désactivée.\n", noCarte);
        }
    }
    else
    {
        printf("Ce numéro d'adhérent n'existe pas. Veuillez réessayer.\n");
        return;
    }
}