# SAE 1.01 - Implémentation d'un besoin client

## Centre aquatique

### Cahier des charges

Il peut s'avérer difficile de gérer les différents clients d'un complexe aquatique. C'est pour cela que nous avons créé cette application, qui permet de gérer les données des clients de Aquavenue.  
L'objectif est de faciliter la gestion des points et des données des clients.
Il existe plusieurs activités :

- Piscine : 0 points (accès libre).
- Cours de natation : 10 points.
- Cours de gymnastique aquatique : 12 points.
- Option accessibilité : 0 points.
- Aquafit : 9 points.
- Aquakid et aquaplouf : 5 points.
- Vagues : 15 points.
- Aquabike : 20 points.
- Aquafamily : 35 points.

Lorsqu'un client décide de faire une activité, il est discrédité du nombre de points correspondants.  
A chaque entrée, le client peut choisir plusieurs activités. Sa carte se fait alors débiter de la somme des points des différentes activités choisies.

**Antoine** : Ouverture.c, Sauvegarde.c, Menus.c, Global.c, GestionAdherents.c, Affichage.c  
**Ayour** : Affichage.c, Menus.c, Global.c, GestionAdherents.c  
**Kyllian** : GestionPoints.c, Global.c, Menus.c, GestionAdherents.c  

### Fonctionnalités

- Membres : chaque membre bénéficie d'une carte qui leur permet d'accéder aux différentes activités et est identifié par son numéro de carte.  
On stocke des informations comme son âge, si sa carte est active ou pas et le nombre de points sur sa carte.
- On peut ajouter et supprimer des clients.
- Il est possible d'ajouter des points sur la carte.  
Chaque point coûte 1 euro.  
Il y a des bonus :  
Lorsque l'on ajoute entre 0 et 20 points : rien,  
Entre 21 et 50 : 5% de points en plus,  
Entre 51 et 100 : 10%,  
Au dessus de 100 : 15%.
- Il est possible de désactiver la carte d'un client, par exemple pour cause de sanction suite à un mauvais comportement, de vol ou de perte.  
On peut aussi la réactiver s'il la retrouve ou que sa sanction est levée.
- On peut afficher les informations d'un seul client ou de tous les clients.
- A chaque fois qu'un client fait une activité, le nombre de fois où une activité à été pratiquée dans la journée augmente de 1.  
On peut afficher ce nombre.  
Il est impossible de pratiquer une activité plus d'une fois par jour.

### Priorité de travail

1. Menu
2. Chargement des fichiers
3. Affichage d'un adhérent
4. Affichage de tous les adhérents
5. Affichage du nombre d'entrées dans la journée
6. Affichage du nombre d'entrées depuis toujours
7. Afficher le nombre d'adhérents
8. Gestion Menu (Global)
9. Recherche d'un client
10. Ajout d'un client
11. Modification de l'âge d'un client
12. Supprimer un client
13. Ajout de points : la carte devient active. Entre 0 et 20 rien, entre 20 et 50 5%, entre 50 et 100 10%, plus 15%
14. Modification de l'activation de la carte ==> Perte, sanction, carte retrouvée
15. Activité faite ==> Perte de points, ajout d'un au nombre d'activités du jour depuis création
16. Sauvegarde
