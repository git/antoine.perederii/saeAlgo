/**
 * \file Ouverture.c
 * \brief Fichier contenant les fonctions d'ouverture des fichiers de données
 * \author Antoine Perederii
*/
#include "Fonctions.h"

/**
 * \brief Appelle les fonctions d'ouverture
 * \author Antoine Perederii
 * \param tNoCarte tableau contenant les numéros de toutes les cartes
 * \param tAge tableau contenant les âges des membres
 * \param tPointsCarte tableau contenant le nombre de points restants sur chaque carte
 * \param tCarteActive tableau contenant l'état de chaque carte
 * \param tNbActivitesJour tableau contenant le nombre d'activité par jour
 * \param tDate tableau contenant les dates
 * \param tMax taille physique des tableaux
 * \param pasAct pointeur contenant la taille logique des tableaux des activités
 * \return la taille logique des tableaux des membres 
*/
int Ouverture(int tNoCarte[], int tAge[], int tPointsCarte[], int tCarteActive[], int tNbActivitesJour[], int tDate[], int tMax, int *pasAct)
{
    int pasMembres;
    pasMembres = OuvertureMembres(tNoCarte, tAge, tPointsCarte, tCarteActive, tMax);
    *pasAct = OuvertureActivitesJour(tNbActivitesJour, tDate, tMax);
    return pasMembres;
}

/**
 * \brief Charge les données du fichier membres.don dans des tableaux
 * \author Antoine Perederii
 * \param tNoCarte tableau contenant les numéros de toutes les cartes
 * \param tAge tableau contenant les âges des membres
 * \param tPointsCarte tableau contenant le nombre de points restants sur chaque carte
 * \param tCarteActive tableau contenant l'état de chaque carte
 * \param tMax taille physique des tableaux
 * \return la taille logique des tableaux
 * 
 * Parcourt chaque champ dans le fichier membres.don et insère dans le tableau correspondant sa valeur.
 * Renvoie un message d'erreur si le fichier n'existe pas.
*/
int OuvertureMembres(int tNoCarte[], int tAge[], int tPointsCarte[], int tCarteActive[], int tMax)
{
    int i = 0;
    int noCarte, age, pointsCarte, carteActive;
    FILE *flot;
    flot = fopen("membres.don", "r");
    if (flot == NULL)
    {
        printf("Problème d'ouverture du fichier membres.don en lecture.\n");
        return -1;
    }
    fscanf(flot, "%d%d%d%d", &noCarte, &age, &pointsCarte, &carteActive);
    while (!feof(flot))
    {
        if (i == tMax)
        {
            printf("Tableau plein.\n");
            fclose(flot);
            return -1;
        }
        tNoCarte[i] = noCarte;
        tAge[i] = age;
        tPointsCarte[i] = pointsCarte;
        tCarteActive[i] = carteActive;
        fscanf(flot, "%d%d%d%d", &noCarte, &age, &pointsCarte, &carteActive);
        i++;
    }
    fclose(flot);
    return i;
}

/**
 * \brief Charge les données du fichier ActivitesJour.don dans des tableaux
 * \author Antoine Perederii
 * \param tNbActivitesJour tableau contenant le nombre d'activité par jour
 * \param tDate tableau contenant les dates
 * \param tMax taille physique des tableaux
 * \return la taille logique des tableaux
 * 
 * Parcourt chaque champ dans le fichier ActivitesJour.don et insère dans le tableau correspondant sa valeur.
 * Renvoie un message d'erreur si le fichier n'existe pas.
*/
int OuvertureActivitesJour(int tNbActivitesJour[], int tDate[], int tMax)
{
    int i = 0;
    int date, nbActivitesJour;
    FILE *jour;
    jour = fopen("ActivitesJour.don", "r");
    if (jour == NULL)
    {
        printf("Problème d'ouverture du fichier ActivitesJour.don en lecture.\n");
        return -1;
    }
    fscanf(jour, "%d%d", &date, &nbActivitesJour);
    while (!feof(jour))
    {
        if (i == tMax)
        {
            printf("Tableau plein.\n");
            fclose(jour);
            return -1;
        }
        tDate[i] = date;
        tNbActivitesJour[i] = nbActivitesJour;
        fscanf(jour, "%d%d", &date, &nbActivitesJour);
        i++;
    }
    fclose(jour);
    return i;
}