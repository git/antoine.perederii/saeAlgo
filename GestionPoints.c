/**
 * \file GestionPoints.c
 * \brief Fichier contenant les fonctions pour la gestion des points des adhérents
 * \author Antoine Perederii - Kyllian Chabanon
 */

#include "Fonctions.h"

/**
 * \brief Recherche un adhérent
 * \author Kyllian Chabanon
 * \param tNoCarte tableau contenant les numéros de toutes les cartes
 * \param nbElem taille logique des tableaux
 * \param noCarte numéro de la carte à rechercher
 * \param trouve pointeur prenant comme valeur 1 si le numéro a été trouvé et 0 sinon
 * \return l'index où se trouve le numéro de la carte recherché, s'il n'y est pas, retourne l'endroit où il doit être inséré
 */
int RechercheAdherent(int tNoCarte[], int nbElem, int noCarte, int *trouve)
{
    int i;
    for (i = 0; i < nbElem; i++)
    {
        if (tNoCarte[i] == noCarte)
        {
            *trouve = 1;
            return i;
        }
        else if (tNoCarte[i] > noCarte)
        {
            *trouve = 0;
            return i + 1;
        }
    }
}

/**
 * \brief Ajoute des points sur une carte
 * \author Kyllian Chabanon
 * \param tNoCarte tableau contenant les numéros de toutes les cartes
 * \param tPointsCarte tableau contenant le nombre de points restants sur chaque carte
 * \param tCarteActive tableau contenant l'état de chaque carte
 * \param nbElem taille logique des tableaux
 * \param noCarte numéro de la carte à créditer
 *
 * Permet d'ajouter des points à la carte correspondant au numéro de carte donné en paramètre.
 * Il y a des bonus donnés selon le nombre de points ajoutés :
 * - En dessous de 20 : aucun bonus
 * - Entre 21 et 50 : 5%
 * - Entre 51 et 100 : 10%
 * - Au dessus de 101 : 15%
 */
void AjoutPoints(int tNoCarte[], int tPointsCarte[], int tCarteActive[], int nbElem, int noCarte)
{
    int pointsCarte, trouve, pas, err = 1, bonus;
    pas = RechercheAdherent(tNoCarte, nbElem, noCarte, &trouve);
    if (trouve == 1)
    {
        printf("Entrez le nombre de points à ajouter : ");
        scanf("%d", &pointsCarte);
        while (err == 1)
        {
            if (pointsCarte > 0 && pointsCarte <= 20)
            {
                tPointsCarte[pas] = tPointsCarte[pas] + pointsCarte;
                printf("Merci pour votre achat. Vous avez crédité la carte n°%d de %d points.\nNombre de points total : %d\n", noCarte, pointsCarte, tPointsCarte[pas]);
                err = 0;
            }
            else if (pointsCarte > 20 && pointsCarte <= 50)
            {
                bonus = pointsCarte * (1 + (5 / 100.0)) - pointsCarte;
                tPointsCarte[pas] += pointsCarte + bonus;
                printf("Merci pour votre achat. Vous avez crédité la carte n°%d de %d points. De plus, nous vous offrons un bonus de 5%%, soit %d points supplémentaires.\nNombre de points total : %d\n", noCarte, pointsCarte, bonus, tPointsCarte[pas]);
                err = 0;
            }
            else if (pointsCarte > 50 && pointsCarte <= 100)
            {
                bonus = pointsCarte * (1 + (5 / 100.0)) - pointsCarte;
                tPointsCarte[pas] += pointsCarte + bonus;
                printf("Merci pour votre achat. Vous avez crédité la carte n°%d de %d points. De plus, nous vous offrons un bonus de 5%%, soit %d points supplémentaires.\nNombre de points total : %d\n", noCarte, pointsCarte, bonus, tPointsCarte[pas]);
                err = 0;
            }
            else if (pointsCarte > 100)
            {
                bonus = pointsCarte * (1 + (5 / 100.0)) - pointsCarte;
                tPointsCarte[pas] += pointsCarte + bonus;
                printf("Merci pour votre achat. Vous avez crédité la carte n°%d de %d points. De plus, nous vous offrons un bonus de 5%%, soit %d points supplémentaires.\nNombre de points total : %d\n", noCarte, pointsCarte, bonus, tPointsCarte[pas]);
                err = 0;
            }
            else
            {
                printf("Le nombre de points est incorrect.\n");
                printf("Veuillez ressaisir le nombre de points à ajouter : ");
                scanf("%d", &pointsCarte);
            }
        }
    }
    else
    {
        printf("Ce numéro d'adhérent n'existe pas. Veuillez réessayer.\n");
        return;
    }
}

/**
 * \brief Débite une carte lorsqu'un adhérent pratique une activité
 * \author Antoine Perederii - Kyllian Chabanon
 * \param tNoCarte tableau contenant les numéros de toutes les cartes
 * \param tNbActivitesJour tableau contenant le nombre d'activité qui a été faite dans la journée
 * \param tCarteActive tableau contenant l'état de chaque carte
 * \param tNbActivitesJour tableau contenant le nombre d'activités du jour
 * \param nbElem taille logique des tableaux
 *
 * Demande à l'adhérent quelle activité il veut faire et retire le nombre de points correspondant.
 */
void DebitCarte(int tNoCarte[], int tPointsCarte[], int tCarteActive[], int tNbActivitesJour[], int nbElem)
{
    int choix, trouve, pas, noCarte;
    char reponse;
    printf("Entrez le numéro de la carte de l'adhérent recherché : ");
    scanf("%d", &noCarte);
    pas = RechercheAdherent(tNoCarte, nbElem, noCarte, &trouve);
    if (trouve == 1)
    {
        if (tCarteActive[pas] == 1)
        {
            choix = choixMenuActivites();
            while (choix != 10)
            {
                switch (choix)
                {
                case 1:
                    if (tPointsCarte[pas] >= 0)
                    {
                        tNbActivitesJour[pas] = tNbActivitesJour[pas] + 1;
                        printf("Vous avez pratiqué la piscine.\n");
                    }
                    else
                    {
                        printf("Vous n'avez pas assez de points.\n");
                    }
                    break;
                case 2:
                    if (tPointsCarte[pas] >= 0)
                    {
                        tPointsCarte[pas] = tPointsCarte[pas] - 0;
                        tNbActivitesJour[pas] = tNbActivitesJour[pas] + 1;
                        printf("Vous avez pratiqué l'option accessibilité.\n");
                    }
                    else
                    {
                        printf("Vous n'avez pas assez de points.\n");
                    }
                    break;
                case 3:
                    if (tPointsCarte[pas] >= 5)
                    {
                        tPointsCarte[pas] = tPointsCarte[pas] - 5;
                        tNbActivitesJour[pas] = tNbActivitesJour[pas] + 1;
                        printf("Vous avez pratiqué Aquakid + Aquaplouf.\n");
                    }
                    else
                    {
                        printf("Vous n'avez pas assez de points.\n");
                    }
                    break;
                case 4:
                    if (tPointsCarte[pas] >= 9)
                    {
                        tPointsCarte[pas] = tPointsCarte[pas] - 9;
                        tNbActivitesJour[pas] = tNbActivitesJour[pas] + 1;
                        printf("Vous avez pratiqué Aquafit.\n");
                    }
                    else
                    {
                        printf("Vous n'avez pas assez de points.\n");
                    }
                    break;
                case 5:
                    if (tPointsCarte[pas] >= 10)
                    {
                        tPointsCarte[pas] = tPointsCarte[pas] - 10;
                        tNbActivitesJour[pas] = tNbActivitesJour[pas] + 1;
                        printf("Vous avez pratiqué Cours de natation.\n");
                    }
                    else
                    {
                        printf("Vous n'avez pas assez de points.\n");
                    }
                    break;
                case 6:
                    if (tPointsCarte[pas] >= 12)
                    {
                        tPointsCarte[pas] = tPointsCarte[pas] - 12;
                        tNbActivitesJour[pas] = tNbActivitesJour[pas] + 1;
                        printf("Vous avez pratiqué Cours de gymnastique aquatique.\n");
                    }
                    else
                    {
                        printf("Vous n'avez pas assez de points.\n");
                    }
                    break;
                case 7:
                    if (tPointsCarte[pas] >= 15)
                    {
                        tPointsCarte[pas] = tPointsCarte[pas] - 15;
                        tNbActivitesJour[pas] = tNbActivitesJour[pas] + 1;
                        printf("Vous avez pratiqué Vagues.\n");
                    }
                    else
                    {
                        printf("Vous n'avez pas assez de points.\n");
                    }
                    break;
                case 8:
                    if (tPointsCarte[pas] >= 20)
                    {
                        tPointsCarte[pas] = tPointsCarte[pas] - 20;
                        tNbActivitesJour[pas] = tNbActivitesJour[pas] + 1;
                        printf("Vous avez pratiqué Aquabike.\n");
                    }
                    else
                    {
                        printf("Vous n'avez pas assez de points.\n");
                    }
                    break;
                case 9:
                    if (tPointsCarte[pas] >= 35)
                    {
                        tPointsCarte[pas] = tPointsCarte[pas] - 35;
                        tNbActivitesJour[pas] = tNbActivitesJour[pas] + 1;
                        printf("Vous avez pratiqué Aquafamily.\n");
                    }
                    else
                    {
                        printf("Vous n'avez pas assez de points.\n");
                    }
                    break;
                }
                choix = choixMenuActivites();
            }
        }
        else
        {
            printf("Votre carte est inactive. Veuillez la réactiver.\n");
        }
    }
    else
    {
        printf("Ce numéro d'adhérent n'existe pas. Veuillez réessayer.\n");
        return;
    }
}