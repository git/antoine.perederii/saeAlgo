/**
 * \file Global.c
 * \brief Fichier contenant la fonction de gestion des menus
 * \author Antoine Perederii - Ayour Malki
*/

#include "Fonctions.h"

/**
 * \brief Gère les menus en appelant une fonction pour chaque choix fait par l'utilisateur
 * \author Antoine Perederii - Ayour Malki
*/
void GestionMenus(void)
{
    int tNoCarte[20] = {0}, tAge[20] = {0}, tPointsCarte[20] = {0}, tCarteActive[20] = {0};
    int tDate[20] = {0}, tNbActivitesJour[20] = {0};
    int tMax = 20, nbElem, choix, pasAct, noCarte;
    char choixA;
    nbElem = Ouverture(tNoCarte, tAge, tPointsCarte, tCarteActive, tNbActivitesJour, tDate, tMax, &pasAct);
    if (nbElem < 0)
    {
        printf("Erreur d'ouverture du fichier ou tableau plein !!!\n");
        return;
    }
    choix = ChoixMenu();
    while (choix != 8)
    {
        if (choix == 1)
        {
            choixA = ChoixMenuAffichage();
            while (choixA != 'F' && choixA != 'f')
            {
                if (choixA == 'A' || choixA == 'a')
                {
                    Affichage1Adherent(tNoCarte, tAge, tPointsCarte, tCarteActive, nbElem);
                }
                else if (choixA == 'B' || choixA == 'b')
                {
                    AffichageTousAdherents(tNoCarte, tAge, tPointsCarte, tCarteActive, nbElem);
                }
                else if (choixA == 'C' || choixA == 'c')
                {
                    AffichageNbEntreesTousJour(tNbActivitesJour, tDate, nbElem);
                }
                else if (choixA == 'D' || choixA == 'd')
                {
                    AffichageNbEntreesTotal(tNbActivitesJour, nbElem);
                }
                else if (choixA == 'E' || choixA == 'e')
                {
                    AffichageNbAdherents(nbElem);
                }
                choixA = ChoixMenuAffichage();
            }
        }
        else if (choix == 2)
        {
            nbElem = AjoutAdherent(tNoCarte, tAge, tPointsCarte, tCarteActive, tMax, nbElem);
        }
        else if (choix == 3)
        {
            ModificationAge(tNoCarte, tAge, nbElem);
        }
        else if (choix == 4)
        {
            nbElem = SupprimerAdherent(tNoCarte, tAge, tPointsCarte, tCarteActive, nbElem);
        }
        else if (choix == 5)
        {
            printf("Entrez le numéro de la carte à créditer : ");
            scanf("%d", &noCarte);
            AjoutPoints(tNoCarte, tPointsCarte, tCarteActive, nbElem, noCarte);
        }
        else if (choix == 6)
        {
            ModificationActivationCarte(tNoCarte, tCarteActive, nbElem);
        }
        else if (choix == 7)
        {
            DebitCarte(tNoCarte, tPointsCarte, tCarteActive, tNbActivitesJour, nbElem);
        }
        choix = ChoixMenu();
    }
    Sauvegarde(tNoCarte, tAge, tPointsCarte, tCarteActive, tNbActivitesJour, tDate, nbElem, pasAct);
    exit(1);
}